import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from './pages/Home.vue'
import Privacy from './pages/Privacy.vue'
import OurRestaurants from './pages/OurRestaurants.vue'
import Careers from './pages/Careers.vue'
import Reviews from './pages/Reviews.vue'
import About from './pages/About.vue'
import SpecialOffers from './pages/SpecialOffers.vue'
import BonusCard from './pages/BonusCard.vue'
import Underconstruction from './pages/Underconstruction.vue'
import ProductMenu from './pages/ProductMenu.vue'

Vue.use(VueRouter)

export default new VueRouter({
	routes: [
		{
			path: '/',
			redirect: '/home'
		},
		{
			path: '/underconstruction',
			name: 'Underconstruction',
			component: Underconstruction
		},
		{
			path: '/home',
			name: 'Home',
			component: Home
		},
		{
			path: '/product_menu',
			name: 'ProductMenu',
			component: ProductMenu
		},
		{
			path: '/privacy',
			name: 'Privacy',
			component: Privacy
		},
		{
			path: '/our_restaurants',
			name: 'OurRestaurants',
			component: OurRestaurants
		},
		{
			path: '/careers',
			name: 'Careers',
			component: Careers
		},
		{
			path: '/reviews',
			name: 'Reviews',
			component: Reviews
		},
		{
			path: '/about',
			name: 'About',
			component: About
		},
		{
			path: '/special_offers',
			name: 'SpecialOffers',
			component: SpecialOffers
		},
		{
			path: '/bonus_card',
			name: 'BonusCard',
			component: BonusCard
		}
	]
})