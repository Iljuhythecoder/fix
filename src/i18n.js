import Vue from 'vue'
import VuewI18n from 'vue-i18n'
import lv from './langs/lv.json'

Vue.use(VuewI18n)

export const i18n = new VuewI18n({
	locale: 'lv',
	fallbackLocale: 'ru',
	messages: {
		lv
	}
})